# milo

![](https://img.shields.io/badge/written%20in-PHP%20%28bootstrap.css%29-blue)

A web-based accounting software package.

Supports multiple users, multiple payee tracking, multiple bill statuses, and email alerts. Backed by an sqlite database.
